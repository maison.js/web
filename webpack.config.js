const TARGET = process.env.npm_lifecycle_event,
	path              = require("path"),
	HtmlWebpackPlugin = require("html-webpack-plugin"),
	OfflinePlugin     = require("offline-plugin"),
	webpack           = require("webpack"),
	package           = require("./package.json"),


	dev               = TARGET === 'build:dev' || TARGET === 'dev' || TARGET === 'dev-ext' || !TARGET

let config = {
	entry: "./src/main.tsx",
	devtool: "source-map",

	output: {
		path    : path.resolve(__dirname, "dist"),
		filename: "main.js"
	},
	resolve: {
		extensions: [".js", ".ts", ".tsx"]
	},
	module: {
		rules: [
			{
				test   : /\.tsx?/,
				loader : "tslint-loader",
				enforce: "pre",
				exclude: [/node_modules/]
			}, {
				test   : /\.tsx?/,
				loader : "ts-loader",
				exclude: [/node_modules/]
			}, {
				test   : /\.less$/,
				enforce: "pre",
				exclude: [/node_modules/],
				use    : [
					{ loader: "style-loader" },
					{ loader: "typings-for-css-modules-loader", options: { // do also css-loader
						sourceMap     : TARGET === 'build:dev' || TARGET === 'dev' || !TARGET,
						modules       : true,
						camelCase     : true,
						namedExport   : true,
						localIdentName: "[name]__[local]___[hash:base64:5]",
						importLoaders : 2
					} },
					{ loader: "postcss-loader", options: {
						plugins: [ require("autoprefixer") ]
					} },
					{ loader: "less-loader", options: {
						sourceMap        : TARGET === 'build:dev' || TARGET === 'dev' || !TARGET,
						javascriptEnabled: true
					} }
				]
			}, {
				test  : /\.svg$/,
				loader: "url-loader"
			}, {
				test: /\.(jpe?g|png|gif)$/i,
				use: [
				  "url-loader?limit=10000"
				]
			}
		]
	},
	plugins: [
		new webpack.WatchIgnorePlugin([
			/(less|css)\.d\.ts$/
		]),
		new HtmlWebpackPlugin({
			title   : package.title.toString(),
			template: "./src/index.html",
			inject  : "body",
			filename: "index.html"
		})
	]
}

console.log("okay:", package.title)

if (dev) {
	config.plugins.push(
		new webpack.DefinePlugin({
			VERSION      : JSON.stringify(package.version),
			PRODUCTION   : JSON.stringify(false),
			DEBUG        : true,
			CODE_FRAGMENT: '80 + 5'
		})
	)

	config.mode = "development";

	console.log("DEV ENV");
} else {
	config.plugins.push(
		new webpack.DefinePlugin({
			VERSION   : JSON.stringify(package.version),
			PRODUCTION: JSON.stringify(true),
			DEBUG     : false
		})
	, new OfflinePlugin({
		responseStrategy: "network-first",
		externals       : ['https://fonts.googleapis.com/*']
	}))

	config.mode = "production";

  console.info('PROD ENV');
}

module.exports = config;