export default interface Color {
	R: number, G: number, B: number
}