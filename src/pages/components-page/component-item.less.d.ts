export const componentItem: string;
export const buttonSwitch: string;
export const deleteButton: string;
export const disabled: string;
export const deleteButtonAnimationEntering: string;
export const deleteButtonAnimationEnteringDone: string;
