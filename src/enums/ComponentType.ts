enum Components {
	ONOFF_SWITCH = 0,
	PWM_SWITCH = 1,
	BUTTON_SWITCH = 2
}

export default Components
