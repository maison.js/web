export default function isHttps() {
	return location.protocol === "https:"
}
