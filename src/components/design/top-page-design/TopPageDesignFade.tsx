import TopPageDesign from "./TopPageDesign"

export default class TopPageDesignFade extends TopPageDesign {
	public componentDidMount() {
		this.div!.style.height = "100%"
	}
}
