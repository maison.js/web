export const rippleContainer: string;
export const ripple: string;
export const rippleAnimation: string;
export const lightRipple: string;
export const blackRipple: string;
