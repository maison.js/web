export const modalBackground: string;
export const modalContent: string;
export const modalAnimationEnterDone: string;
export const mobile: string;
export const modalBar: string;
export const title: string;
export const buttons: string;
export const icon: string;
