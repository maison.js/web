export const extendedInput: string;
export const focus: string;
export const hasLabel: string;
export const error: string;
export const errorContainer: string;
