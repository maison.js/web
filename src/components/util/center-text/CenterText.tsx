import * as React from "react"

import classnames from "classnames"
import { centerText } from "./centertext.less"

interface ICenterTextProps {
	className?: string,
	children: React.ReactNode
}

export default class CenterText extends React.Component<ICenterTextProps> {
	public render() {
		return <div className={ classnames(this.props.className, centerText) }>
			{ this.props.children }
		</div>
	}
}
