export const mobile: string;
export const headerAnimation: string;
export const headerAnimationActive: string;
export const headerButtonContainer: string;
export const headerButton: string;
export const headerOverlay: string;
export const navHeader: string;
export const titleContainer: string;
