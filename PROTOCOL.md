# Protocole

Ce fichier décrit le protocole employé pour échanger les informations entre la partie Web et R.Pi.

Connexion en websockets - Port 80.

**IMPORTANT**: Chaque objet envoyé et reçu a un identifiant unique, `id`, fourni par le client. Le serveur doit retouner une réponse avec ce même id.

Tout est en objets json:

- Connexion
	- web -> pi :
		```{"type": "hello"}```
	- pi -> web:
		```json
		{
			"type": "hello",
			"name": "Son prénom", // ce serait cool qu'on peut lui donner un prénom
			"configured": true, // si le raspberry pi est déjà configuré. si non, voir #Configuration.
			"passwordRequired": true, // mot de passe demandé ou pas
		}
		```

	-> Voir Connexion.

- Configuration
	- web -> pi :
		```json
		{
			"type": "configuration"
		}
	- pi -> web, si déjà configuré :
		- Abort mission:
			```json
			{
				"error": true,
				"errorId": 5
			}
			```
		- ```json
		 	{
				"status": "waiting"
			}
			```
	- web -> pi :
		```json
		{
			"type": "configuration",
			"name": "un nom",
			"password": false, // peut être égal soit à false, soit à un sha256 du mdp
		}
		```
	- pi -> web :
		```json
		{
			"status": "configured"
		}
		```

- Connexion
	- web -> pi :
	```json
	{
		"type": "login",
		"password": "dsqdqs", // si il y a un mot de passe, ceci est un sha256 du mot de passe.
	}
	```

	- pi -> web :
	```json
	{
		"type": "login",
		"result": true, // si le mot de passe est juste, la connexion est autorisée.
		"cooldown": 200, // si le mot de passe est faux, un cooldown peut être appliqué, en secondes, recommencer la connexion
	}
	```
## Erreurs

* 5 - R.Pi déjà configuré